﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using EShop.Entities;
using EShop.StorageContracts;

namespace EShop.MemoryStorage
{
    public class OrderMemoryRepository: IOrderRepository
    {
        protected readonly ConcurrentDictionary<string, OrderDetails> Orders = new ConcurrentDictionary<string, OrderDetails>();

        public Task AddOrder(OrderDetails order)
        {
            if (order == null)
            {
                throw new ArgumentNullException(nameof(order));
            }

            if (string.IsNullOrEmpty(order.OrderId))
            {
                throw new ArgumentException("Invalid id", nameof(order));
            }

            var res = Orders.TryAdd(order.OrderId, order);
            if (!res)
            {
                throw new InvalidOperationException("Order already exists");
            }

            return Task.CompletedTask;
        }

        public Task<OrderDetails> GetOrder(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
            {
                throw new ArgumentException("Invalid id", nameof(orderId));
            }

            if (Orders.TryGetValue(orderId, out var order))
            {
                return Task.FromResult(order);
            }
            else
            {
                throw new InvalidOperationException($"Order with id '{orderId}' doesn't exist");
            }
        }
    }
}
