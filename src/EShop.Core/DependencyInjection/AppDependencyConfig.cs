﻿using EShop.Business.Contracts;
using EShop.Business.Services;
using EShop.MemoryStorage;
using EShop.StorageContracts;
using Microsoft.Extensions.DependencyInjection;

namespace EShop.Core.DependencyInjection
{
    public class AppDependencyConfig
    {
        public void RegisterAppDependencies(IServiceCollection services)
        {
            services.AddTransient<IOrderService, OrderService>();
            services.AddSingleton<IOrderRepository, OrderMemoryRepository>(); //It is singleton only for memory repositories
            services.AddTransient<IProductOrderFactory, ProductOrderFactory>();
        }
    }
}
