﻿namespace EShop.Entities.ProductOrders
{
    public class CardsOrder : ProductOrderBase
    {
        public override string Title => "Greeting CardsOrder";

        protected override double PackageWidth => 4.7;

        public CardsOrder(int quantity) : base(quantity)
        {
        }
    }
}