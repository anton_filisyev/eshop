﻿namespace EShop.Entities.ProductOrders
{
    public class CanvasOrder : ProductOrderBase
    {
        public override string Title => "CanvasOrder";

        protected override double PackageWidth => 16;

        public CanvasOrder(int quantity) : base(quantity)
        {
        }
    }
}