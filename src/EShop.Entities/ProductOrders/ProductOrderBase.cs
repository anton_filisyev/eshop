﻿using System;

namespace EShop.Entities.ProductOrders
{
    public abstract class ProductOrderBase
    {
        /// <summary>
        /// Product title
        /// </summary>
        public abstract string Title { get; }

        /// <summary>
        /// Quantity
        /// </summary>
        public int Quantity { get; set; }

        /// <summary>
        /// Package width in mm
        /// </summary>
        protected abstract double PackageWidth { get; }

        /// <summary>
        /// Max number of items in a single stack
        /// </summary>
        protected virtual int StackSize => 1;

        /// <summary>
        /// Get package min width of the order
        /// </summary>
        /// <returns></returns>
        public virtual double GetPackageWidth()
        {
            var stacks = (int) Math.Ceiling(1.0 * Quantity / StackSize);
            return PackageWidth * stacks;
        }

        protected ProductOrderBase(int quantity)
        {
            Quantity = quantity;
        }
    }
}
