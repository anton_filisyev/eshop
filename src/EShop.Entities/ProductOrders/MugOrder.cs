﻿namespace EShop.Entities.ProductOrders
{
    public class MugOrder : ProductOrderBase
    {
        public override string Title => "MugOrder";

        protected override int StackSize => 4;

        protected override double PackageWidth => 94;

        public MugOrder(int quantity) : base(quantity)
        {
        }
    }
}