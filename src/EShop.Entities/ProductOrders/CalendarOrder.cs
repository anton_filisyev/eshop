﻿namespace EShop.Entities.ProductOrders
{
    public class CalendarOrder : ProductOrderBase
    {
        public override string Title => "CalendarOrder";

        protected override double PackageWidth => 10;

        public CalendarOrder(int quantity) : base(quantity)
        {
        }
    }
}