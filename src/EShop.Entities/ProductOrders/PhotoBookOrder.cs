﻿namespace EShop.Entities.ProductOrders
{
    public class PhotoBookOrder: ProductOrderBase
    {
        public override string Title => "Photo Book";

        protected override double PackageWidth => 19;

        public PhotoBookOrder(int quantity) : base(quantity)
        {
        }
    }
}