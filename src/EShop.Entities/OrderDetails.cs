﻿using System.Collections.Generic;
using EShop.Entities.ProductOrders;

namespace EShop.Entities
{
    public class OrderDetails
    {
        public string OrderId { get; set; }

        public IList<ProductOrderBase> ProductOrders { get; set; }

        public double RequiredBinWidth { get; set; }
    }
}