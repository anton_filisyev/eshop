﻿using System.Collections.Generic;

namespace EShop.Entities
{
    public class OrderRequest
    {
        public string OrderId { get; set; }

        public IList<ProductRequest> ProductRequests { get; set; }

        public OrderRequest()
        {
            ProductRequests = new List<ProductRequest>();
        }
    }
}
