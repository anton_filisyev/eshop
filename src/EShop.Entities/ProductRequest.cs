﻿namespace EShop.Entities
{
    public class ProductRequest
    {
        public ProductType ProductType { get; set; }
        public int Quantity { get; set; }
    }
}
