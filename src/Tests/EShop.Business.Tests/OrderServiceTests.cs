using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EShop.Business.Contracts;
using EShop.Business.Services;
using EShop.Entities;
using EShop.Entities.ProductOrders;
using EShop.StorageContracts;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;

namespace EShop.Business.Tests
{
    public class OrderServiceTests
    {
        [Test]
        public void AddOrder_WhenInvalidData_ShouldFail()
        {
            var loggerMock = new Mock<ILogger<OrderService>>();
            var repositoryMock = new Mock<IOrderRepository>();
            var productOrderFactory = new Mock<IProductOrderFactory>();

            var orderService = new OrderService(repositoryMock.Object, loggerMock.Object, productOrderFactory.Object);

            Assert.ThrowsAsync<ArgumentNullException>(async () => await orderService.AddOrder(null));
        }

        [TestCase(null)]
        [TestCase("")]
        public void AddOrder_WhenInvalidId_ShouldFail(string orderId)
        {
            var loggerMock = new Mock<ILogger<OrderService>>();
            var repositoryMock = new Mock<IOrderRepository>();
            var productOrderFactory = new Mock<IProductOrderFactory>();

            var orderService = new OrderService(repositoryMock.Object, loggerMock.Object, productOrderFactory.Object);

            var order = new OrderRequest()
            {
                OrderId = orderId
            };

            var ex = Assert.ThrowsAsync<ArgumentException>(async () => await orderService.AddOrder(order));
            Assert.That(ex.Message, Does.StartWith("Invalid id"));
        }

        [Test]
        public void AddOrder_WhenInvalidProductList_ShouldFail()
        {
            var loggerMock = new Mock<ILogger<OrderService>>();
            var repositoryMock = new Mock<IOrderRepository>();
            var productOrderFactory = new Mock<IProductOrderFactory>();

            var orderService = new OrderService(repositoryMock.Object, loggerMock.Object, productOrderFactory.Object);

            var order = new OrderRequest()
            {
                OrderId = "OrderId",
                ProductRequests = new List<ProductRequest>()
            };

            var ex = Assert.ThrowsAsync<ArgumentException>(async () => await orderService.AddOrder(order));
            Assert.That(ex.Message, Does.StartWith("Invalid product list"));
        }

        [TestCase(1, 1, 113)]
        [TestCase(1, 4, 113)]
        [TestCase(1, 5, 207)]
        [TestCase(2, 8, 226)]
        [TestCase(1, 0, 19)]
        [TestCase(4, 0, 76)]
        public async Task AddOrder_WhenValidData_ShouldReturnOrderDetails(int photoBoolQuantity, int mugQuantity, double expectedPackageWidth)
        {
            //Arrange
            var loggerMock = new Mock<ILogger<OrderService>>();
            var repositoryMock = new Mock<IOrderRepository>();
            var productOrderFactory = new Mock<IProductOrderFactory>();
            productOrderFactory
                .Setup(mock =>
                    mock.CreateProductOrder(It.Is<ProductType>(x => x == ProductType.PhotoBook), It.Is<int>(x => x == photoBoolQuantity)))
                .Returns(new PhotoBookOrder(photoBoolQuantity));
            productOrderFactory
                .Setup(mock =>
                    mock.CreateProductOrder(It.Is<ProductType>(x => x == ProductType.Mug), It.Is<int>(x => x == mugQuantity)))
                .Returns(new MugOrder(mugQuantity));

            var orderService = new OrderService(repositoryMock.Object, loggerMock.Object, productOrderFactory.Object);
            
            var order = new OrderRequest()
            {
                OrderId = Guid.NewGuid().ToString(),
                ProductRequests = new List<ProductRequest>()
            };

            if (photoBoolQuantity > 0)
            {
                order.ProductRequests.Add(new ProductRequest()
                {
                    ProductType = ProductType.PhotoBook,
                    Quantity = photoBoolQuantity
                });
            }

            if (mugQuantity > 0)
            {
                order.ProductRequests.Add(new ProductRequest()
                {
                    ProductType = ProductType.Mug,
                    Quantity = mugQuantity
                });
            }

            //Act
            var orderDetails = await orderService.AddOrder(order);

            //Assert
            Assert.IsNotNull(orderDetails);
            Assert.That(orderDetails.OrderId, Is.EqualTo(order.OrderId));
            Assert.IsNotNull(orderDetails.ProductOrders);
            Assert.That(orderDetails.ProductOrders.Count, Is.EqualTo(order.ProductRequests.Count));
            Assert.That(orderDetails.RequiredBinWidth, Is.EqualTo(expectedPackageWidth));

            repositoryMock.Verify(
                mock => mock.AddOrder(It.Is<OrderDetails>(x => x.OrderId == order.OrderId && x.ProductOrders.Count == order.ProductRequests.Count)),
                Times.Once());
        }
    }
}