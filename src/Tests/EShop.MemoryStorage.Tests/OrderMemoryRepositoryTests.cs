using System;
using System.Collections.Generic;
using System.Linq;
using EShop.Entities;
using EShop.Entities.ProductOrders;
using NUnit.Framework;

namespace EShop.MemoryStorage.Tests
{
    public class OrderMemoryRepositoryTests
    {
        private OrderMemoryRepository _repository;

        [SetUp]
        public void Setup()
        {
            _repository = new OrderMemoryRepository();
        }

        #region AddOrder

        [Test]
        public void AddOrder_WhenInvalidData_ShouldFail()
        {
            Assert.Throws<ArgumentNullException>(() => _repository.AddOrder(null));
        }

        [TestCase(null)]
        [TestCase("")]
        public void AddOrder_WhenInvalidId_ShouldFail(string orderId)
        {
            var order = new OrderDetails()
            {
                OrderId = orderId
            };

            var ex = Assert.Throws<ArgumentException>(() => _repository.AddOrder(order));
            Assert.That(ex.Message, Does.StartWith("Invalid id"));
        }

        [Test]
        public void AddOrder_WhenAlreadyExists_ShouldFail()
        {
            string orderId = Guid.NewGuid().ToString();
            var order = new OrderDetails()
            {
                OrderId = orderId
            };

            _repository.AddOrder(order);

            var anotherOrder = new OrderDetails()
            {
                OrderId = orderId
            };

            var ex = Assert.Throws<InvalidOperationException>(() => _repository.AddOrder(anotherOrder));
            Assert.That(ex.Message, Does.StartWith("Order already exists"));
        }

        [Test]
        public void AddOrder_WhenValidData_ShouldBeStored()
        {
            string orderId = Guid.NewGuid().ToString();
            var order = new OrderDetails()
            {
                OrderId = orderId,
                ProductOrders = new List<ProductOrderBase>()
                {
                    new CanvasOrder(2),
                    new PhotoBookOrder(1)
                }
            };

            _repository.AddOrder(order);

            var storedOrder = _repository.GetOrder(orderId).Result;
            Assert.IsNotNull(storedOrder);
            Assert.IsNotNull(storedOrder.ProductOrders);
            Assert.That(storedOrder.OrderId, Is.EqualTo(orderId));
            Assert.That(storedOrder.ProductOrders.Count, Is.EqualTo(2));
        }

        #endregion

        #region GetOrder

        [TestCase(null)]
        [TestCase("")]
        public void GetOrder_WhenInvalidId_ShouldFail(string orderId)
        {
            var ex = Assert.Throws<ArgumentException>(() => _repository.GetOrder(orderId));
            Assert.That(ex.Message, Does.StartWith("Invalid id"));
        }

        [Test]
        public void GetOrder_WhenDoesNotExist_ShouldBeNull()
        {
            string orderId = Guid.NewGuid().ToString();

            var ex = Assert.Throws<InvalidOperationException>(() => _repository.GetOrder(orderId));
            Assert.That(ex.Message, Does.StartWith($"Order with id '{orderId}' doesn't exist"));
        }
        
        [Test]
        public void GetOrder_WhenExist_ShouldHasCorrectData()
        {
            string orderId = Guid.NewGuid().ToString();
            var canvasOrder = new CanvasOrder(3);
            var photoBookOrder = new PhotoBookOrder(2);
            var order = new OrderDetails()
            {
                OrderId = orderId,
                ProductOrders = new List<ProductOrderBase>() { canvasOrder, photoBookOrder }
            };

            _repository.AddOrder(order);

            var storedOrder = _repository.GetOrder(orderId).Result;
            Assert.IsNotNull(storedOrder);
            Assert.IsNotNull(storedOrder.ProductOrders);
            Assert.That(storedOrder.OrderId, Is.EqualTo(orderId));
            Assert.That(storedOrder.OrderId, Is.EqualTo(orderId));
            Assert.That(storedOrder.ProductOrders.Count, Is.EqualTo(2));

            var storedCanvasOrder = storedOrder.ProductOrders.FirstOrDefault(x => x is CanvasOrder);
            Assert.IsNotNull(storedCanvasOrder);
            Assert.That(storedCanvasOrder.Quantity, Is.EqualTo(canvasOrder.Quantity));

            var storedPhotoBookOrder = storedOrder.ProductOrders.FirstOrDefault(x => x is PhotoBookOrder);
            Assert.IsNotNull(storedPhotoBookOrder);
            Assert.That(storedPhotoBookOrder.Quantity, Is.EqualTo(photoBookOrder.Quantity));
        }

        #endregion
    }
}