﻿using System.Threading.Tasks;
using EShop.Business.Contracts;
using EShop.Entities;
using Microsoft.AspNetCore.Mvc;

namespace EShop.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET api/<OrderController>/5
        [HttpGet("{orderId}")]
        public async Task<OrderDetails> Get(string orderId)
        {
            return await _orderService.GetOrder(orderId);
        }

        // POST api/<OrderController>
        [HttpPost]
        public async Task<OrderDetails> Post([FromBody] OrderRequest orderRequest)
        {
            return await _orderService.AddOrder(orderRequest);
        }
    }
}
