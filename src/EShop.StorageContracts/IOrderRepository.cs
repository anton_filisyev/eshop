﻿using System.Threading.Tasks;
using EShop.Entities;

namespace EShop.StorageContracts
{
    public interface IOrderRepository
    {
        /// <summary>
        /// Add a new order to the storage
        /// </summary>
        /// <param name="order">Order to add</param>
        Task AddOrder(OrderDetails order);

        /// <summary>
        /// Get order by id. Throw an exception if order is not found
        /// </summary>
        /// <param name="orderId">order id</param>
        /// <returns>Order</returns>
        Task<OrderDetails> GetOrder(string orderId);

        //Out of scope
        // Task UpdateOrder(Order order);
        // Task RemoveOrder(string orderId);
    }
}
