﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EShop.Business.Contracts;
using EShop.Entities;
using EShop.Entities.ProductOrders;
using EShop.StorageContracts;
using Microsoft.Extensions.Logging;

namespace EShop.Business.Services
{
    public class OrderService: IOrderService
    {
        private readonly ILogger<OrderService> _logger;
        private readonly IOrderRepository _orderRepository;
        private readonly IProductOrderFactory _productOrderFactory;

        public OrderService(IOrderRepository orderRepository, ILogger<OrderService> logger, IProductOrderFactory productOrderFactory)
        {
            _orderRepository = orderRepository;
            _logger = logger;
            _productOrderFactory = productOrderFactory;
        }

        public async Task<OrderDetails> AddOrder(OrderRequest orderRequest)
        {
            if (orderRequest == null)
            {
                throw new ArgumentNullException(nameof(orderRequest));
            }

            if (string.IsNullOrEmpty(orderRequest.OrderId))
            {
                throw new ArgumentException("Invalid id", nameof(orderRequest));
            }

            if (orderRequest.ProductRequests == null || orderRequest.ProductRequests.Count == 0)
            {
                throw new ArgumentException("Invalid product list", nameof(orderRequest));
            }

            try
            {
                var productOrders = orderRequest.ProductRequests
                    .Select(x => _productOrderFactory.CreateProductOrder(x.ProductType, x.Quantity)).ToList();
                var packageWidth = GetPackageWidth(productOrders);

                var orderDetails = new OrderDetails()
                {
                    OrderId = orderRequest.OrderId,
                    ProductOrders = productOrders,
                    RequiredBinWidth = packageWidth
                };

                await _orderRepository.AddOrder(orderDetails);

                return orderDetails;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error on saving new order");
                throw;
            }
        }

        public async Task<OrderDetails> GetOrder(string orderId)
        {
            try
            {
                return await _orderRepository.GetOrder(orderId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error on getting order with id {orderId}");
                throw;
            }
        }

        private double GetPackageWidth(IEnumerable<ProductOrderBase> productOrders)
        {
            return productOrders.Sum(x => x.GetPackageWidth());
        }
    }
}
