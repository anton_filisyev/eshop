﻿using System;
using EShop.Business.Contracts;
using EShop.Entities;
using EShop.Entities.ProductOrders;

namespace EShop.Business.Services
{
    public class ProductOrderFactory: IProductOrderFactory
    {
        public ProductOrderBase CreateProductOrder(ProductType productType, int quantity)
        {
            switch (productType)
            {
                case ProductType.PhotoBook: return new PhotoBookOrder(quantity);
                case ProductType.Calendar: return new CalendarOrder(quantity);
                case ProductType.Canvas: return new CanvasOrder(quantity);
                case ProductType.Cards: return new CardsOrder(quantity);
                case ProductType.Mug: return new MugOrder(quantity);
                default:
                    throw new InvalidOperationException($"Product type '{productType}' is not supported");
            }
        }
    }
}
