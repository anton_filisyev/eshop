﻿using EShop.Entities;
using EShop.Entities.ProductOrders;

namespace EShop.Business.Contracts
{
    public interface IProductOrderFactory
    {
        /// <summary>
        /// create product object for corresponding product type
        /// </summary>
        /// <param name="productType">product type to create</param>
        /// <param name="quantity">quantity</param>
        /// <returns>product</returns>
        ProductOrderBase CreateProductOrder(ProductType productType, int quantity);
    }
}