﻿using System.Threading.Tasks;
using EShop.Entities;

namespace EShop.Business.Contracts
{
    public interface IOrderService
    {
        /// <summary>
        /// Add new order
        /// </summary>
        /// <param name="orderRequest">Order to add</param>
        /// <returns>Order details</returns>
        Task<OrderDetails> AddOrder(OrderRequest orderRequest);

        /// <summary>
        /// Get order by id
        /// </summary>
        /// <param name="orderId">Order id</param>
        /// <returns>Order details</returns>
        Task<OrderDetails> GetOrder(string orderId);
    }
}
